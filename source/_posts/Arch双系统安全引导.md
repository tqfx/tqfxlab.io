---
title: Arch双系统安全引导
date: 2023-04-06 15:19:54
categories: Arch
tags: Arch
excerpt: Windows + Arch 启用 Secure Boot (安全启动)
---

## 步骤

开启安装模式（清除安全密钥），重新安装引导

```bash
grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=Arch --modules="tpm" --disable-shim-lock
```

生成配置文件

```bash
grub-mkconfig -o /boot/grub/grub.cfg
```

安装配置工具

```bash
pacman -S sbctl
```

查询安装模式

```bash
sbctl status
```

创建密钥

```bash
sbctl create-keys
```

注册密钥

```bash
sbctl enroll-keys -m
```

查询状态

```bash
sbctl status
```

检测签名前的状态

```bash
sbctl verify
```

签名引导

```bash
sbctl sign -s /boot/efi/EFI/Arch/grubx64.efi
```

检测签名后的状态

```bash
sbctl verify
```

重启系统并开启安全启动，查询状态

```bash
sbctl status
```

## 参考

<https://wiki.tqfx.org/static/Arch%2F安全引导>
<https://wiki.tqfx.org/#Arch%2F安全引导>
