---
title: 一阶低通滤波器
date: 2024-01-05 16:19:54
categories: 滤波器
tags: 滤波器
math: true
excerpt: 低频通过，高频衰减。
---

## 推导

![ ](/images/1st_order_RC_low_pass_filter.svg)

数学模型
$$
\tag{1}RC\frac{\text{d}V_{\mathrm o}}{\text{dt}}+V_{\mathrm o}=V_{\mathrm i}
$$
拉氏变换
$$
(RCs+1)U_{\mathrm o}=U_{\mathrm i}
$$
传递函数
$$
H(s)=\frac{1}{RCs+1}
$$

令 $s=j\omega$, 得
$$
H(j\omega)=\frac{1}{(RC\omega)^2+1}-\frac{RC\omega}{(RC\omega)^2+1}j=X(\omega)+Y(\omega)j
$$
频率特性
$$
H(j\omega)=|H(j\omega)|e^{j\angle H(j\omega)}=A(\omega)e^{j\varphi(\omega)}
$$
幅频特性
$$
A(\omega)=\sqrt{\left[X(\omega)\right]^2+\left[Y(\omega)\right]^2}=\frac{1}{\sqrt{(RC\omega)^2+1}}
$$
相频特性
$$
\varphi(\omega)=\arctan\frac{Y(\omega)}{X(\omega)}=-\arctan(RC\omega)
$$

已知截止频率 $\omega_c = 2\pi f_c$
$$
A(\omega_c)=\frac{1}{\sqrt{(RC\omega_c)^2+1}}=\frac{\sqrt{2}}{2}
$$
得
$$
RC=\frac{1}{\omega_c}=\frac{1}{2\pi f_c}
$$

对式(1)采用一阶向后差分进行离散化
$$
RC\frac{V_{\mathrm o}(n)-V_{\mathrm o}(n-1)}{T_s}+V_{\mathrm o}(n)=V_{\mathrm i}(n)
$$
整理得到
$$
V_{\mathrm o}(n)=\frac{RC}{RC+T_s}V_{\mathrm o}(n-1)+\frac{T_s}{RC+T_s}V_{\mathrm i}(n)
$$
其中, $T_s$ 为采样频率, $RC=\frac{1}{2\pi f_c}$

令
$$
\alpha=\frac{T_s}{RC+T_s}=\frac{2\pi f_c T_s}{2\pi f_c T_s+1}
$$
得
$$
V_{\mathrm o}(n)=(1-\alpha)V_{\mathrm o}(n-1)+\alpha V_{\mathrm i}(n)
$$

## 仿真

<script src="https://gist.github.com/tqfx/ae8c667a7626a5bee161edc4d469c78b.js"></script>

```sh
git clone https://gist.github.com/ae8c667a7626a5bee161edc4d469c78b
cd ae8c667a7626a5bee161edc4d469c78b
cmake -B build
cmake --build build
build/main > main.log
gnuplot -c main.gp main.log
```

## 参考

<https://en.wikipedia.org/wiki/Low-pass_filter>
