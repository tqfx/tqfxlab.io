---
title: Bash 快捷键
date: 2023-09-17 16:27:25
categories: Bash
tags: Bash
excerpt: 使用Bash的快捷键提升命令行效率
---

## 编辑命令

* `Ctrl + A`：移到命令行首
* `Ctrl + E`：移到命令行尾
* `Ctrl + F`：按字符前移（右向）
* `Ctrl + B`：按字符后移（左向）
* `Alt + F`：按单词前移（右向）
* `Alt + B`：按单词后移（左向）
* `Ctrl + XX`：在命令行首和光标之间移动
* `Ctrl + U`：从光标处删除至命令行首
* `Ctrl + K`：从光标处删除至命令行尾
* `Ctrl + W`：从光标处删除至字首
* `Alt + D`：从光标处删除至字尾
* `Ctrl + D`：删除光标处的字符
* `Ctrl + H`：删除光标前的字符
* `Ctrl + Y`：粘贴至光标后
* `Alt + C`：从光标处更改为首字母大写的单词
* `Alt + U`：从光标处更改为全部大写的单词
* `Alt + L`：从光标处更改为全部小写的单词
* `Ctrl + T`：交换光标处和之前的字符
* `Alt + T`：交换光标处和之前的单词
* `Alt + BackSpace`：与 `Ctrl + W` 类似

## 重新执行命令

* `Ctrl + R`：逆向搜索命令历史
* `Ctrl + G`：从历史搜索模式退出
* `Ctrl + P`：历史中的上一条命令
* `Ctrl + N`：历史中的下一条命令
* `Alt + .`：使用上一条命令的最后一个参数

## 控制命令

* `Ctrl + L`：清屏
* `Ctrl + O`：执行当前命令，并选择上一条命令
* `Ctrl + S`：阻止屏幕输出
* `Ctrl + Q`：允许屏幕输出
* `Ctrl + C`：终止命令
* `Ctrl + Z`：挂起命令

## Bang (!) 命令

* `!!`：执行上一条命令
* `!foo`：执行最近的以 `foo` 开头的命令，如 `!ls`
* `!foo:p`：仅打印输出，而不执行
* `!$`：上一条命令的最后一个参数，与 `Alt + .` 相同
* `!$:p`：打印输出 `!$` 的内容
* `!*`：上一条命令的所有参数
* `!*:p`：打印输出 `!*` 的内容
* `^foo`：删除上一条命令中的 `foo`
* `^foo^bar`：将上一条命令中的 `foo` 替换为 `bar`
* `^foo^bar^`：将上一条命令中所有的 `foo` 都替换为 `bar`

## 参考

<https://wiki.tqfx.org/static/Bash%2F%E5%BF%AB%E6%8D%B7%E9%94%AE>
<https://wiki.tqfx.org/#Bash%2F%E5%BF%AB%E6%8D%B7%E9%94%AE>
