---
title: Lua中实现table的打印
date: 2024-01-12 10:56:25
categories: Lua
tags: Lua
excerpt: Lua自带的print函数只能打印可转化为字符串的数据，这里提供了几种可以打印table的实现。
---

## io版

```lua
local m = {}
m.indent = "    "
function m.dir(...)
    local io_write_cache = {}
    local function io_write(indent, x)
        local str = tostring(x)
        if io_write_cache[str] then
            io.write(indent, "*", str, "\n")
        else
            io_write_cache[str] = true
            if type(x) == "table" then
                for idx, val in pairs(x) do
                    if type(idx) ~= "string" then
                        idx = "[" .. tostring(idx) .. "]"
                    elseif idx:find('"') then
                        idx = '["' .. idx:gsub('"', '\\"') .. '"]'
                    end
                    if type(val) == "table" then
                        io.write(indent, idx, " = ", str, "\n")
                        io.write(indent, "{\n")
                        io_write(indent .. m.indent, val)
                        io.write(indent, "}\n")
                    else
                        if type(val) ~= "string" then
                            val = tostring(val)
                        else
                            val = '"' .. val:gsub('"', '\\"') .. '"'
                        end
                        io.write(indent, idx, " = ", val, ",\n")
                    end
                end
            else
                io.write(indent, str, "\n")
            end
        end
    end
    for _, x in ipairs({ ... }) do
        if type(x) == "table" then
            io.write(tostring(x), "\n")
            io.write("{\n")
            io_write(m.indent, x)
            io.write("}\n")
        else
            io_write("", x)
        end
    end
end
function m.log(...)
    for i, v in ipairs({ ... }) do
        if i > 1 then
            io.write(" ")
        end
        io.write(tostring(v))
    end
    io.write("\n")
end
function m.fmt(...)
    io.write(string.format(...), "\n")
end
return m
```

## print版

```lua
local m = {}
m.log = print
m.indent = "    "
function m.dir(...)
    local m_log_cache = {}
    local function m_log(indent, x)
        local str = tostring(x)
        if m_log_cache[str] then
            m.log(indent .. "*" .. str)
        else
            m_log_cache[str] = true
            if type(x) == "table" then
                for idx, val in pairs(x) do
                    if type(idx) ~= "string" then
                        idx = "[" .. tostring(idx) .. "]"
                    elseif idx:find('"') then
                        idx = '["' .. idx:gsub('"', '\\"') .. '"]'
                    end
                    if type(val) == "table" then
                        m.log(indent .. idx .. " = " .. str)
                        m.log(indent .. "{")
                        m_log(indent .. m.indent .. val)
                        m.log(indent .. "}")
                    else
                        if type(val) ~= "string" then
                            val = tostring(val)
                        else
                            val = '"' .. val:gsub('"', '\\"') .. '"'
                        end
                        m.log(indent .. idx .. " = " .. val .. ",")
                    end
                end
            else
                m.log(indent .. str)
            end
        end
    end
    for _, x in ipairs({ ... }) do
        if type(x) == "table" then
            m.log(x)
            m.log("{")
            m_log(m.indent, x)
            m.log("}")
        else
            m_log("", x)
        end
    end
end
function m.fmt(...)
    m.log(string.format(...))
end
return m
```

## 参考

<script src="https://gist.github.com/tqfx/073168b27e7f93261d1dfda1f511dc75.js"></script>
