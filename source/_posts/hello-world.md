---
title: 'Hello, world!'
categories: 代码
tags: 代码
index_img: /images/index/hello-world.png
banner_img: /images/banner/hello-world.jpg
excerpt: 你好，世界！
math: true
mermaid: true
date: 2021-05-28 13:50:00
---

## 安装

首先将npm的官方源更换为国内源来加速安装

```bash
npm config set registry https://registry.npmmirror.com
```

然后使用以下命令全局安装hexo的命令行程序

```bash
npm install -g hexo-cli
```

## 建站

```bash
hexo init blog
cd blog
npm install
```

## 配置

<https://hexo.io/zh-cn/docs/configuration>

## 主题

<https://fluid-dev.github.io/hexo-fluid-docs/start>

```bash
rm _config.landscape.yml
npm uninstall hexo-theme-landscape
npm install --save hexo-theme-fluid
npm install --save hexo-generator-about
```

```yml
language: zh-CN
theme: fluid
```

```bash
cp node_modules/hexo-theme-fluid/_config.yml _config.fluid.yml
```

## C

```c
#include <stdio.h>
int main(void)
{
    puts("Hello, world!");
    return 0;
}
```

```c
int puts(char const *); int main(void) { puts("Hello, world!"); return 0; }
```

## C++

```cpp
#include <iostream>
int main(void) {
    std::cout << "Hello, world!" << std::endl;
    return 0;
}
```

## Rust

```rs
fn main() {
    println!("Hello, world!");
}
```

## Python

```py
print("Hello, world!")
```

## Math

$$
E=mc^2
$$

光速$c=299792458$m/s

## Mermaid

```mermaid
graph TD
A-->B
A-->C
B-->D
C-->D
```
