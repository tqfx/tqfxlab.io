---
title: 一阶高通滤波器
date: 2024-01-05 16:20:07
categories: 滤波器
tags: 滤波器
math: true
excerpt: 高频通过，低频衰减。
---

## 推导

![ ](/images/1st_order_CR_high_pass_filter.svg)

数学模型
$$
\tag{1}RC\frac{\text{d}(V_{\mathrm i}-V_{\mathrm o})}{\text{dt}}=V_{\mathrm o}
$$
拉氏变换
$$
RCs(U_{\mathrm i}-U_{\mathrm o})=U_{\mathrm o}
$$
传递函数
$$
H(s)=\frac{RCs}{RCs+1}
$$

令 $s=j\omega$, 得
$$
H(j\omega)=\frac{(RC\omega)^2}{(RC\omega)^2+1}+\frac{RC\omega}{(RC\omega)^2+1}j=X(\omega)+Y(\omega)j
$$
频率特性
$$
H(j\omega)=|H(j\omega)|e^{j\angle H(j\omega)}=A(\omega)e^{j\varphi(\omega)}
$$
幅频特性
$$
A(\omega)=\sqrt{\left[X(\omega)\right]^2+\left[Y(\omega)\right]^2}=\frac{RC\omega}{\sqrt{(RC\omega)^2+1}}
$$
相频特性
$$
\varphi(\omega)=\arctan\frac{Y(\omega)}{X(\omega)}=\arctan(\frac{1}{RC\omega})
$$

已知截止频率 $\omega_c = 2\pi f_c$
$$
A(\omega_c)=\frac{RC\omega}{\sqrt{(RC\omega_c)^2+1}}=\frac{\sqrt{2}}{2}
$$
得
$$
RC=\frac{1}{\omega_c}=\frac{1}{2\pi f_c}
$$

对式(1)采用一阶向后差分进行离散化
$$
RC\frac{[V_{\mathrm i}(n)-V_{\mathrm i}(n-1)]-[V_{\mathrm o}(n)-V_{\mathrm o}(n-1)]}{T_s}=V_{\mathrm o}(n)
$$
整理得到
$$
V_{\mathrm o}(n)=\frac{RC}{RC+T_s}[V_{\mathrm o}(n-1)+V_{\mathrm i}(n)-V_{\mathrm i}(n-1)]
$$
其中, $T_s$ 为采样频率, $RC=\frac{1}{2\pi f_c}$

令
$$
\alpha=\frac{RC}{RC+T_s}=\frac{1}{2\pi f_c T_s + 1}
$$
得
$$
V_{\mathrm o}(n)=\alpha[V_{\mathrm o}(n-1)+V_{\mathrm i}(n)-V_{\mathrm i}(n-1)]
$$

## 仿真

<script src="https://gist.github.com/tqfx/c5a032ce61f5dabc6d2e57ed08f46a14.js"></script>

```sh
git clone https://gist.github.com/c5a032ce61f5dabc6d2e57ed08f46a14
cd c5a032ce61f5dabc6d2e57ed08f46a14
cmake -B build
cmake --build build
build/main > main.log
gnuplot -c main.gp main.log
```

## 参考

<https://en.wikipedia.org/wiki/High-pass_filter>
