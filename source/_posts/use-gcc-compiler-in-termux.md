---
title: Termux安装GCC编译器
date: 2023-08-19 14:35:13
categories: Termux
tags: Termux
banner_img: /images/banner/Termux.png
excerpt: 介绍在Termux环境中安装GNU/GCC工具链的方法
---

在Termux环境中安装GNU/GCC工具链有下列几种方法:

## tur

```bash
pkg update
pkg install -y tur-repo
pkg install gcc-default-13
```

## cctools

```bash
pkg update
pkg install -y coreutils gnupg
```

```bash
curl -sLo $PREFIX/etc/apt/trusted.gpg.d/cctools.asc --create-dirs https://cctools.info/public.key
```

```bash
echo "deb [trusted=yes] https://cctools.info termux cctools" | tee $PREFIX/etc/apt/sources.list.d/cctools.list
```

```bash
pkg update
pkg install gcc-cctools ndk-sysroot-cctools-api-26-$(uname -m)
```

## pointless

```bash
pkg update
pkg install -y coreutils gnupg
```

```bash
curl -sLo $PREFIX/etc/apt/trusted.gpg.d/pointless.gpg --create-dirs https://its-pointless.github.io/pointless.gpg
```

```bash
echo "deb [trusted=yes] https://its-pointless.github.io/files/24 termux extras" | tee $PREFIX/etc/apt/sources.list.d/pointless.list
```

```bash
pkg update
pkg install -y gcc-11
```

## 参考

<https://github.com/termux-user-repository/tur>
<https://android.stackexchange.com/questions/182130>
